/* Simple conuter that counts up on the positive edge of a clock signal */

module counter1 (
    clk,
    q
);
    parameter COUNTER_WIDTH = 4;

    input clk;
    output [COUNTER_WIDTH-1:0] q;

    reg [COUNTER_WIDTH-1:0] cntr = 0;
    assign q = cntr;

    always @ (posedge clk) begin
        cntr <= cntr + 1;
    end

endmodule
