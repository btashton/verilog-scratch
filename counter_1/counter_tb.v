/* Simple test bench for counter1 */

module counter1_tb;
    parameter COUNTER_WIDTH = 4;
    reg clk;
    wire [COUNTER_WIDTH-1:0] cntr;

    counter1 #(COUNTER_WIDTH) counter(
        .clk(clk),
        .q(cntr)
    );

    initial begin
        clk = 0;
    end

    always
        #5 clk = !clk;


    initial begin
        $dumpfile("counter.vcd");
        $dumpvars;
    end

    initial begin
        $display("\t\ttime,\tclk,\tcntr");
        $monitor("%d\t%d\t%d", $time, clk, cntr);
    end

    initial begin
        #200 $finish;
    end

endmodule