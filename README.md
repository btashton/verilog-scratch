# Verilog Examples

This is a dumping ground for code I am writing as I re-learn Verilog.

Most of the examples should be able to be simulated via Icarus Verilog.
Some later examples might pull in MyHDL which I want to explore for building
more complex test suites.

## Current Examples

 * counter1 -- Simple up counter with parameter to set the width

These examples can be executed via make, for example:

```
$ make counter1_test
vvp counter1
VCD info: dumpfile counter.vcd opened for output.
                time,   clk,    cntr
                   0    0        0
                   5    1        1
                  10    0        1
                  15    1        2
                  20    0        2
                  25    1        3
                  30    0        3
                  35    1        4
                  40    0        4
                  45    1        5
                  50    0        5
                  55    1        6
                  60    0        6
                  65    1        7
                  70    0        7
                  75    1        8
                  80    0        8
                  85    1        9
                  90    0        9
                  95    1       10
                 100    0       10
                 105    1       11
                 110    0       11
                 115    1       12
                 120    0       12
                 125    1       13
                 130    0       13
                 135    1       14
                 140    0       14
                 145    1       15
                 150    0       15
                 155    1        0
                 160    0        0
                 165    1        1
                 170    0        1
                 175    1        2
                 180    0        2
                 185    1        3
                 190    0        3
                 195    1        4
                 200    0        4
```
