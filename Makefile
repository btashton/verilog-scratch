VVP := vvp
IV := iverilog


counter1: counter_1/counter.v counter_1/counter_tb.v
	$(IV) -o counter1 counter_1/counter.v counter_1/counter_tb.v

counter1.vcd: counter1
	$(VVP) counter1
	
counter1_test: counter1.vcd
	
clean:
	rm -rf counter1
	rm -rf counter1.vcd